<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.robusta.io" prefix="robusta"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<jsp:useBean id="fora" class="io.robusta.fora.bean.ForaBean"
	scope="application"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>All comments</title>
 <link rel="stylesheet" href="css/fora.css" type="text/css">
</head>
<body>

<h1>Comments : ${fora.allComments.size() }</h1>

 
	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>User</th>
				<th>Content</th>
			</tr>
		</thead>
		
		
		
		<c:forEach items="${fora.allComments}" var="comment">
			<robusta:comment comment="${comment}"></robusta:comment>
		</c:forEach>
	</table>

	<h2>Creating a new Comment</h2>
	<form action="" method="POST">
	
		Topic ID : <input type="number" value="1" name="topicId"><br/>
		<textarea rows="10" cols="30" name="content" placeholder="Your Comment"></textarea><br/>
		Email : <input type="email" value="jo@robusta.io" name="email"><br/>
		<input type="submit" value="OK">
	
	</form>
</body>
</html>