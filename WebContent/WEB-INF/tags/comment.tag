<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="comment" type="io.robusta.fora.domain.Comment"
	required="true"%>

<tr data-comment="${comment.id}">
	<td>${comment.id}</td>
	<td>${comment.user}</td>
	<td>${comment.content}</td>
	<td data-user="${comment.user.id}"><img src="images/user.png" /></td>
	<td><img src="images/like.png" data-like="true"/></td>
	<td><img src="images/dislike.png" data-like="false"/></td>
	<td><img src="images/flag.jpg" /></td>
</tr>
