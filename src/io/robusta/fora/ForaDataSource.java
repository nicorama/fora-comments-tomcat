package io.robusta.fora;

import io.robusta.fora.domain.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ForaDataSource {

	// ids for topics
	public static final long TROLL_ID = 1L;
	public static final long US_ID = 2L;
	public static final long SPICE_ID = 3L;
	
	protected static ForaDataSource instance;

	List<Tag> tags = new ArrayList<Tag>();
	List<User> users = new ArrayList<User>();
	List<Topic> topics = new ArrayList<Topic>();
	List<Comment> comments = new ArrayList<Comment>();
	List<Flag> flags = new ArrayList<Flag>();
	
	
	private Topic troll;
	private Topic games;
	private Topic spices;

	private Tag violence;
	private Tag fun;
	private Tag science;
	
	private Admin nicolas, leonard;

	private ForaDataSource() {

		System.out.println("creating object");
		initDataSource();
		//fillMany(24);
	}
	
	public static ForaDataSource getInstance() {
		if (instance == null) {
			instance = new ForaDataSource();
		}
		return instance;
	}

	void initDataSource() {

		initTags();
		initUsers();
		initTopics();
		initComments();
		
	}

	
	
private void initTags() {
		
		
		
		violence = new Tag(1L, "Violence");
		fun = new Tag(2L, "Fun");
		science = new Tag(3L, "Science");
		
		Collections.addAll(this.tags, violence, fun, science);


	}

	private void initUsers() {
		User nicolas = new Admin("Star Wars rocks !");
		nicolas.setId(1L);
		nicolas.setEmail("nz@robusta.io");
		nicolas.setName("Nicolas");

		User leonard = new Admin("Star Trek rocks");
		leonard.setId(2L);
		leonard.setName("Leonard");
		leonard.setEmail("leonard@robusta.io");

		User sheldon = new User();
		sheldon.setId(3L);
		sheldon.setName("Sheldon");
		sheldon.setEmail("sheldon@robusta.io");

		User raj = new User();
		raj.setId(4L);
		raj.setName("Raj");
		raj.setEmail("raj@robusta.io");

		User howard = new User();
		howard.setId(5L);
		howard.setName("Howard");
		howard.setEmail("howard@robusta.io");

		User penny = new User();
		penny.setId(6L);
		penny.setName("Penny");
		penny.setEmail("penny@robusta.io");

		User emy = new User();
		emy.setId(7L);
		emy.setName("Emy");
		emy.setEmail("emy@robusta.io");

		User bernie = new User();
		bernie.setId(8L);
		bernie.setName("Bernadette");
		bernie.setEmail("bernie@robusta.io");

		emy.setFemale();
		bernie.setFemale();
		penny.setFemale();
		
		Collections.addAll(this.users, nicolas, leonard, sheldon, raj, howard, penny, emy, bernie);
	}

	private void initTopics() {
		troll = new Topic();
		troll.setId(TROLL_ID);
		troll.setTitle("Star Trek > Star Wars");
		troll.setUser(leonard());

		games= new Topic();
		games.setId(2L);
		games.setTitle("American Football is the best game");		
		games.setUser(penny());
		games.getTags().add(fun);
		
		spices = new Topic();
		spices.setId(3L);
		spices.setTitle("Ketchup is not a Spice");		
		spices.setUser(emy());
		
		Collections.addAll(this.topics, troll, games, spices);
	}
	
	private void initComments() {
		initTrollComments();
		initGamesComments();
		initSpicesComments();
	}

	public List<User> getUsers() {
		return this.users;
	}

	public User nicolas() {
		return this.getUsers().get(0);
	}

	public User leonard() {
		return this.getUsers().get(1);
	}

	public User sheldon() {
		return this.getUsers().get(2);
	}
	
	public User raj() {
		return this.getUsers().get(3);
	}
	
	public User howard() {
		return this.getUsers().get(4);
	}
	
	public User penny() {
		return this.getUsers().get(5);
	}
	
	public User emy() {
		return this.getUsers().get(6);
	}
	
	public User bernie() {
		return this.getUsers().get(7);
	}
	

	public List<Topic> getTopics() {
		return this.topics;
	}

	public List<Comment> getComments() {

		return this.comments;
	}

	private List<Comment> initTrollComments() {

		Comment c1 = new Comment();
		c1.setId("aer3-765");
		c1.setAnonymous(true);
		c1.setContent("I'm not ok");

		Comment c2 = new Comment();
		c2.setId("jgh-76");
		c2.setUser(leonard());
		c2.setContent("You don't know enough about heroes");

		Comment c3 = new Comment();
		c3.setId("3");
		c3.setAnonymous(true);
		c3.setContent("What ? You stupid !");

		Flag flag = new Flag();
		flag.setId(1L);
		flag.setContent("This guy went too far.");
		c3.getFlags().add(flag);
		flags.add(flag);
		
		c3.getTags().add(violence);

		
		Collections.addAll(this.flags,flag);
		Collections.addAll(this.comments, c1,c2,c3);
		return troll.addComments(c1,c2,c3);
	}
	
	private List<Comment> initGamesComments() {

		Comment c1 = new Comment(penny(), "There are so many strategies");		
		Comment c2 = new Comment(leonard(), "What ? These guys are stupid !");
		Comment c3 = new Comment(penny(), "They know how to count to 4");
		Comment c4 = new Comment(sheldon(), "So why do they call it football and play with hands ?");
		
		Collections.addAll(this.comments, c1,c2,c3, c4);
		return games.addComments(c1, c2, c3, c4);

		
	}
	
	private List<Comment> initSpicesComments() {

		Comment c1 = new Comment(emy(), "It misses spices, let's add ketchup");		
		Comment c2 = new Comment(raj(), "What ? You stupid ! It's not a spice !");
		Comment c3 = new Comment(emy(), "But there is spicy vinegar");
		Comment c4 = new Comment(bernie(), "Vinegar is not a spice, it's a fruit");
		Comment c5 = new Comment(emy(), "A liquid fruit ? Doesn't make sense !");
		Comment c6 = new Comment(sheldon(), "And it is a bit <strong>violent</strong> <script type='text/javascript'>alert('you are fired!')</script>");
		
		Collections.addAll(this.comments, c1,c2,c3, c4, c5, c6);
		return spices.addComments(c1, c2, c3, c4, c5, c6);

		
	}
	

	public List<Comment> getTrollComments() {
		return troll.getComments();
	}

	public List<Flag> getFlags() {
		return flags;
	}

	
	public List<Tag> getTags() {
		return tags;
	}
	
	public int getTotalCommentsCount() {
		return comments.size();
	}

	public void fillMany(int size) {
		// int size = 24;
		int userSize = size;
		int topicSize = userSize * 3;
		int commentSize = userSize * 12;
		int flagSize = userSize;

		for (int i = 0; i < userSize; i++) {
			User u = new User();
			u.setEmail("user" + i + "@fora.com");
			u.setName("John Doe - " + i);
			users.add(u);
		}

		for (int i = 0; i < topicSize; i++) {
			Topic s = new Topic();
			s.setTitle("Title " + i);
			User u = getRandomItem(User.class, users);
			s.setUser(u);
			topics.add(s);
		}

		for (int i = 0; i < commentSize; i++) {
			Comment c = new Comment();
			c.setUser(getRandomItem(User.class, users));
			c.setContent("My comment says " + i);
			Topic s = getRandomItem(Topic.class, topics);
			s.getComments().add(c);
			comments.add(c);
		}

		for (int i = 0; i < flagSize; i++) {
			Flag flag = new Flag();
			Comment c = getRandomItem(Comment.class, comments);
			flag.setContent("This is a flag for `" + c.getContent() + "`");
			c.getFlags().add(flag);
			flags.add(flag);
		}

	}

	public <T> T getRandomItem(Class<T> clazz, List<T> list) {

		int length = list.size();

		try {
			int index = new Random().nextInt(length);
			return list.get(index);
		} catch (RuntimeException e) {
			System.out.println("length is " + length);
			throw e;
		}

	}

}
