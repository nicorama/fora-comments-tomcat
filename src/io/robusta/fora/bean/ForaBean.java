package io.robusta.fora.bean;

import io.robusta.fora.ForaDataSource;
import io.robusta.fora.domain.Comment;

import java.util.List;

public class ForaBean {

	static ForaDataSource fora = ForaDataSource.getInstance();
	
	public ForaBean() {
		
	}
	public List<Comment> getTrollComments(){
		return fora.getTrollComments();
	}
	
	public List<Comment>getAllComments(){
		return fora.getComments();
	}
	
	public ForaDataSource getFora() {
		return fora;
	}
	
}
