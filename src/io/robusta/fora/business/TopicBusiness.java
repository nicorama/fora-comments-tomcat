package io.robusta.fora.business;

import io.robusta.fora.ForaDataSource;
import io.robusta.fora.domain.Topic;

import java.util.List;
import java.util.logging.Logger;

public class TopicBusiness {



	ForaDataSource fora = ForaDataSource.getInstance();

	public Topic getTopicById(long id) {

		for (Topic s : fora.getTopics()) {
			if (s.getId() == id) {
				return s;
			}
		}

		return null;// or throw exception
	}

	public List<Topic> getAllTopics() {
		return fora.getTopics();

	}

	public int countTopics() {

		return fora.getTopics().size();

	}
}
