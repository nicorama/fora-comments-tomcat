package io.robusta.fora.business;

import io.robusta.fora.ForaDataSource;
import io.robusta.fora.domain.HasTag;
import io.robusta.fora.domain.Tag;

public class TagBusiness {

	public void tag(String name, HasTag hasTag){
		Tag tag = new Tag(name);
		
		//We could check if the tag already exists
		hasTag.getTags().add(tag);
		ForaDataSource.getInstance().getTags().add(tag);
		
	}
	
}
