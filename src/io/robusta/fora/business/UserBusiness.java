package io.robusta.fora.business;

import io.robusta.fora.ForaDataSource;
import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Topic;
import io.robusta.fora.domain.User;

import java.util.List;
import java.util.logging.Logger;

public class UserBusiness {

	ForaDataSource fora = ForaDataSource.getInstance();

	public User getOrCreateUser(String email) {

		for (User u : fora.getUsers()) {
			if (u != null && u.getEmail() != null && u.getEmail().equalsIgnoreCase(email)) {
				return u;
			}
		}
		return createUser(email);

	}

	
	public List<User> getUsers() {
		return fora.getUsers();
	}

	public User getUserById(long id) {
		for (User u : fora.getUsers()) {
			if (u != null && u.getId() == id) {
				return u;
			}
		}
		return null;
	}

	public User createUser(String email) {
		User u = new User();
		u.setEmail(email);

		u.setId((long) fora.getUsers().size());
		fora.getUsers().add(u);

		return u;
	}

	public void updateUser(User user) {
		
			for (int i = 0; i < fora.getUsers().size(); i++) {
				User u = fora.getUsers().get(i);

				if (u != null && u.getId() == user.getId()) {
					// replacing
					fora.getUsers().remove(i);
					fora.getUsers().add(i, user);
				}
			}
		
	}

	public void deleteUser(User user) {
		ForaDataSource.getInstance().getUsers().remove(user);
		for (Topic topic : ForaDataSource.getInstance().getTopics()) {
			if (topic.getUser().equals(user)) {
				topic.setUser(null);
			}
		}

		for (Comment comment : ForaDataSource.getInstance().getComments()) {
			if (comment.getUser().equals(user)) {
				comment.setUser(null);
				comment.setAnonymous(true);
			}
		}
	}

}
