package io.robusta.fora.servlets;

import io.robusta.fora.business.CommentBusiness;
import io.robusta.fora.domain.Comment;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import javax.servlet.RequestDispatcher;

@WebServlet("/comments/like")
public class LikeServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

            //response.getOutputStream().print("You like it !");
        /*JsonReader reader = new JsonReader(new InputStreamReader(request.getInputStream()));
         JsonParser parser = new JsonParser();
         JsonObject obj = (JsonObject)parser.parse(reader); */
        String id = request.getParameter("id");
        Comment c = new CommentBusiness().getCommentById(id);

        boolean like = Boolean.valueOf(request.getParameter("like"));
                //new Boolean(true); Boolean.valueOf(true);

        if (like) {
            c.up();
        } else {
            c.down();
        }

        /*RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/topics.jsp");
        dispatcher.forward(request, response);*/
        response.sendRedirect("/fora-web/jsp/topics.jsp");

    }

}

class Result {

    String id;
    int score;

    public Result(String id, int score) {
        this.id = id;
        this.score = score;
    }
}
