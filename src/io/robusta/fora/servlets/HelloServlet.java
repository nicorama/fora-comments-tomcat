/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.robusta.fora.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nicolas
 */
@WebServlet("/hello")
public class HelloServlet extends HttpServlet implements ServletRequestListener, ServletContextListener{

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(">>>>>Hello Service");
        super.service(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {        
        
        String userAgent = req.getHeader("User-Agent");
        resp.addHeader("Hey", "It's me ; I'm blowing your mind");
        resp.getOutputStream().print("<html><head></head><body>Hellllllllo "+userAgent + "<br/>"
        + " From "+getServletContext().getInitParameter("user")+"</body></html>");
        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String statement = req.getParameter("statement");
        String size = req.getParameter("size");
        int sizeInt = 0;
        try{
            sizeInt = Integer.parseInt(size);
        }catch(Exception e){
            
        }
        String message;
        if( statement == null || statement.length()<10){
            message = "too short";
        }else{
            message =  "you typed : "+statement + " ; size :"+size;
        }
        resp.getOutputStream().print("<html><head></head><body>Hello Formulaire with POST method : " + message + "</body></html>");
    }



	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("APpplication destroyed");
		
	}



	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("APpplication initialized");
	}



	@Override
	public void requestDestroyed(ServletRequestEvent event) {

		System.out.println("request destroyed");
		
	}



	@Override
	public void requestInitialized(ServletRequestEvent event) {
		System.out.println("request initialized "+event.getServletRequest().getRemoteAddr());
		
	}
    
    
    

    
   
}
