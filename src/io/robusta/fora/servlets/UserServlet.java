package io.robusta.fora.servlets;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.robusta.fora.ForaDataSource;
import io.robusta.fora.business.TopicBusiness;
import io.robusta.fora.business.UserBusiness;
import io.robusta.fora.domain.Admin;
import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Topic;
import io.robusta.fora.domain.User;

import javax.security.auth.Subject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Nicolas
 * Date: 15/02/14
 * Time: 22:26
 */
@WebServlet("/user/*")
public class UserServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        String[] splits = uri.split("/");
        int id = Integer.valueOf(splits[splits.length - 1]);

        User user = new UserBusiness().getUserById(id);
        String json = new Gson().toJson(user);
        resp.setContentType("application/json");
        resp.getOutputStream().print(json);
    }


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Gson gson = new Gson();

        JsonElement document = new JsonParser().parse(new InputStreamReader(req.getInputStream()));
        System.out.println("Updating user " + document.toString());
        User user;
        if (document.isJsonObject() && document.getAsJsonObject().get("admin").getAsBoolean() == true){
            Admin admin = gson.fromJson(document, Admin.class);

            if (admin.getStatement() == null || admin.getStatement().isEmpty()){
                resp.setStatus(406);
                resp.getOutputStream().print("An admin must have a valid statement");
                return;
            }
            user = admin;
        }else{
            user = gson.fromJson(document, User.class);
        }
        new UserBusiness().updateUser(user);

        resp.setContentType("application/json");
        resp.setStatus(200);
        resp.getOutputStream().print("{result:'OK'}");


    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        String[] splits = uri.split("/");
        int id = Integer.valueOf(splits[splits.length - 1]);

        UserBusiness business = new UserBusiness();
        User user = business.getUserById(id);
        if (user!=null){
            business.deleteUser(user);
        }else{
            resp.setStatus(406);
            resp.getOutputStream().print("No user found with id "+id);
            return;
        }

        resp.setContentType("application/json");
        resp.setStatus(200);
        resp.getOutputStream().print("{result:'OK'}");
    }


}
