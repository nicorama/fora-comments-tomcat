package io.robusta.fora.servlets;

import com.google.gson.Gson;
import io.robusta.fora.ForaDataSource;
import io.robusta.fora.business.TopicBusiness;
import io.robusta.fora.domain.Topic;
import io.robusta.fora.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Nicolas
 * Date: 15/02/14
 * Time: 22:22
 */
@WebServlet("/users")
public class UsersServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<User> users = ForaDataSource.getInstance().getUsers();
        String json = new Gson().toJson(users);
        resp.setContentType("application/json");
        resp.getOutputStream().print(json);
    }



}
