package io.robusta.fora.servlets;

import io.robusta.fora.ForaDataSource;
import io.robusta.fora.business.CommentBusiness;
import io.robusta.fora.business.TopicBusiness;
import io.robusta.fora.business.UserBusiness;
import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Topic;
import io.robusta.fora.domain.User;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/comments")
public class CommentsServlet extends HttpServlet implements Filter{

	
	private static final long serialVersionUID = 7961193740608043469L;
	ForaDataSource fora = ForaDataSource.getInstance();
	private final  static Logger logger = Logger.getLogger(CommentsServlet.class.getName()); 
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		List<Comment> comments = fora.getComments() ;
		Gson gson = new Gson();		
		resp.getOutputStream().print(gson.toJson(comments));
		
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		resp.setContentType("text/html;charset=utf-8");
		Long topicId = -1L;
		try{
			topicId = new Long(req.getParameter("topic-id"));
		}catch(Exception ex){
			resp.setStatus(406);
			resp.getOutputStream().print("<html>Topic id not found or not valid</html>");
			return;
		}
		String content = req.getParameter("content");
		String email = req.getParameter("email");
		//String anonymous = new Boolean(req.getParameter("anonymous"));
		Topic s = new TopicBusiness().getTopicById(topicId);
		
		if (s == null){
			resp.setStatus(404);
			resp.getOutputStream().print("<html>Topic not found</html>");
			return;
		}else{
			User user = new UserBusiness().getOrCreateUser(email);
			Comment c = new CommentBusiness().createComment(s, content, user, false);
			resp.setStatus(201);
			resp.getOutputStream().print("Comment created with id "+c.getId());			
		}

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
}
