package io.robusta.fora.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class Topic implements HasTag, Flagable {

	
	long id;
	String title;
	
	List<Comment> comments = new ArrayList<Comment>();
	List<Flag> flags = new ArrayList<Flag>();
	List<Tag>tags = new ArrayList<Tag>();
	
	User user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	// Using id and title
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Topic other = (Topic) obj;
		if (id != other.id)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public boolean isFlagged() {

		return this.flags != null && !this.flags.isEmpty();
	}

	@Override
	public List<Flag> getFlags() {
		return this.flags;
	}

	public void setFlags(List<Flag> flags) {
		this.flags = flags;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public List<Comment> addComments(Comment... comments){
		Collections.addAll(this.comments, comments);
		return Arrays.asList(comments);
	}

	@Override
	public boolean isTagged() {
		return this.tags.size()>0;
	}
	@Override
	public List<Tag> getTags() {
		return this.tags;
	}

	

}
