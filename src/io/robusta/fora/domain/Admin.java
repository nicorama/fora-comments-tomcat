package io.robusta.fora.domain;

/**
 * Created by Nicolas
 * Date: 15/02/14
 * Time: 21:26
 */
public class Admin extends User {

    String statement;

    public Admin(String statement) {
        this.statement = statement;
        this.admin = true;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }
}
